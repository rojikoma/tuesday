#include <stdlib.h>

void mergeSort(int low, int high, int* values)
{
    int mid = (high + low) / 2;
    
    mergeSort(low, mid, values);
    mergeSort(mid+1, high, values);
    merge(low, mid, mid + 1, high, values);
}

void merge(int low, int mid1, int mid2, int high, int* values)
{
    int i = low;
    int j = mid2;
    int k = low;
    int* temp;
    int sz;

    sz = sizeof(&values) / sizeof(int);

    temp = malloc(sizeof(int) * sz);

    while (i <= mid1 || j <= high)
    {
        if (values[i] < values[j])
        {
            temp[k] = values[i];
            i++; k++;
        }
        else if (values[j] < values[i])
        {
            temp[k] = values[j];
            j++; k++;
        }
        
        if (j > high)
        {
            for(i = 0; i < high; i++)
                temp[k++] = values[i];
        }
        else if (i > mid1)
        {
            for(j = 0; j < high; j++)
                temp[k++] = values[j];
        }
        
        for(i = 0; i<=sz; i++)
            values[i] = temp[i];
    }
}

int main()
{
    int a[8] = {12,4,6,2,9,7,3,1};
    int *parray;
    int i;

    mergeSort(0, 7, parray);
    
    for(i = 0; i < 8; i++)
        printf("%d,", a[i]);
    
    return 0;
}
